Le jeu de la vie est un automate cellulaire imaginé par John Horton Conway en 1970 qui est probablement, au début du xxie siècle, le plus connu de tous les automates cellulaires. Malgré des règles très simples, le jeu de la vie est Turing-complet.

Malgré son nom, le jeu de la vie n'est pas un jeu au sens ludique ni au sens de la théorie des jeux.