package jeu;

/**
 * ArgumentException est la classe permettant de gérer les erreurs d'arguments 
 * lors du choix des options du jeu.
 * 
 */

public class ArgumentException extends Exception {
	public ArgumentException(String message) {
		super(message);
	}
}