package jeu;

import java.util.Iterator;
/**
 * <b>EtudeAsymptote est la classe qui calcule le type d'évolution d'un jeu de la vie.</b>
 * Elle s'interesse à cinq (5) types d'évolution du jeu de la vie qui sont : </br>
 * <li>L'état mort.</li>
 * <li>La stabilité.</li>
 * <li>L'oscillation.<li>
 * <li>Vaisseau.<li>
 * <li>L'état inconnu</li>
 * <p>
 * @see JeuDeLaVie
 * @author koceila,serhat
 */


public class EtudesAsymptote {
	private Monde verif_stable;
	private Monde verif_oscillateur;
	private Monde verif_vaisseau;
	public EtudesAsymptote(Monde a, String file) {
		if (a instanceof MondeCirculaire) {
			verif_stable = new MondeCirculaire(file, 2);
			verif_oscillateur = new MondeCirculaire(file, 2);
			verif_vaisseau = new MondeCirculaire(file, 2);
		} else {
			if (a instanceof MondeFrontiere) {
				verif_stable = new MondeFrontiere(file, 3);
				verif_oscillateur = new MondeFrontiere(file, 3);
				verif_vaisseau = new MondeFrontiere(file, 3);

			} else {
				verif_stable = new Monde(file, 1);
				verif_oscillateur = new Monde(file, 1);
				verif_vaisseau = new Monde(file, 1);
			}
		}
	}
/**
 * 
 * @param a
 */
	public EtudesAsymptote(Monde a) {
		verif_stable = new Monde();
		verif_oscillateur = new Monde();
		verif_vaisseau = new Monde();
	}
	/**
	 * true si l etat est mort
	 * @param a
	 * @return boolean
	 */
	public boolean Mort(Monde a) {
		return a.getLemonde().isEmpty();
	}
	/**
	 * verifie si l etat est stable
	 * @param a
	 * @return boolean
	 */
	public boolean Stable(Monde a) {
			verif_stable.nextGeneration();
			return a.getLemonde().equalsTwoList(a.getLemonde(), verif_stable.getLemonde());
	}
	/**
	 * Vérifie que le type d'évolution du jeu est Oscillateur.
	 * @return
	 * 			Un booléen indiquant si le type d'évolution est de type Oscillateur.
	 */

	public boolean Oscillateur(Monde a) {
		verif_oscillateur.nextGeneration();
		verif_oscillateur.nextGeneration();
		return a.getLemonde().equalsTwoList(a.getLemonde(), verif_oscillateur.getLemonde());
	}
	/**
	 * Vérifie que le type d'évolution du jeu est Vaisseau.
	 * @return
	 * 			Un booléen indiquant si le type d'évolution est de type Vaisseau.
	 */

	public boolean Vaisseau(Monde a) {
		verif_vaisseau.nextGeneration();
		verif_vaisseau.nextGeneration();
		return verif_deplacement(a.getLemonde(), verif_vaisseau.getLemonde());
	}
/**
 * 
 * @param liste
 * @param vaisseau
 * @return boolean
 */
	public boolean verif_deplacement(List<Cellule> liste, List<Cellule> vaisseau) {
		boolean identique;
		Iterator<Cellule> it1 = liste.iterator();
		Iterator<Cellule> it2 = vaisseau.iterator();

		if (liste.getSize() == vaisseau.getSize()) {
			Cellule cell1 = (Cellule) it1.next();
			Cellule cell2 = (Cellule) it2.next();
			int deplacementX = cell1.getCoord().x - cell2.getCoord().x;
			int deplacementY = cell1.getCoord().y - cell2.getCoord().y;
			identique = true;
			while (it1.hasNext() && identique) {
				Cellule c1 = (Cellule) it1.next();
				Cellule c2 = (Cellule) it2.next();
				if (c1.getCoord().x - c2.getCoord().x != deplacementX
						|| c1.getCoord().y - c2.getCoord().y != deplacementY) {
					identique = false;
				}
			}
			return identique;

		} else {
			return false;

		}

	}
	/**
	 * methode permer de calculer la periode
	 * @param  a
	 * 
	 * @return periode
	 * 
	 */

	public int calculePeriode(Monde a) {
		List<Cellule> liste = new List<Cellule>();
		int periode = 1;
		Iterator<Cellule> it = a.getLemonde().iterator();
		while (it.hasNext()) {
			Cellule cell = (Cellule) it.next();
			liste.add(cell);

		}
		a.nextGeneration();
		while (!liste.equalsTwoList(a.getLemonde(),liste)) {
			a.nextGeneration();
			periode++;
		}

		return periode;
	}
	
	/**
	 * <p>methode permer de calculer la periode du vaisseau </p>
	 * @param  a
	 * 
	 * @return periode
	 * 
	 */

	public int calculePeriodeVaisseau(Monde a) {
		List<Cellule> liste = new List<Cellule>();
		int periode =1;
		Iterator<Cellule> it = a.getLemonde().iterator();
		while (it.hasNext()) {
			Cellule cell = (Cellule) it.next();
			liste.add(cell);
		}
		a.nextGeneration();
		while (!verif_deplacement(liste, a.getLemonde())) {
			a.nextGeneration();
			periode++;
		}

		return periode;
	}

	/**
	 * <p> methode permer de calculer le deplacement </p>
	 * @param  a
	 * 
	 * @return x
	 * 
	 */
	public int deplacementX(Monde a) {
		Iterator<Cellule> it1 = a.getLemonde().iterator();
		Iterator<Cellule> it2 = verif_vaisseau.getLemonde().iterator();
		return (it2.next().getCoord().x) - (it1.next().getCoord().x);
	}
	/**
	 * <p> methode permer de calculer le deplacement </p>
	 * @param a
	 * 
	 * @return y
	 * 
	 */
	public int deplacementY(Monde a) {
		Iterator<Cellule> it1 = a.getLemonde().iterator();
		Iterator<Cellule> it2 = verif_vaisseau.getLemonde().iterator();
		return (it2.next().getCoord().y) - (it1.next().getCoord().y);
	}

	/**
	 * <p> methode permer de calculer le deplacement </p>
	 * @param  a
	 * 
	 * @return y
	 * 
	 */
	public String deplacementVaisseau(Monde a) {
		return "(abscisse=" + deplacementX(a) + ", ordonnée=" + deplacementY(a) + ")";
	}

}
