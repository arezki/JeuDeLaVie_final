package jeu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.Timer;

/**
 * <b>Jeu est la classe qui gère les paramètre de lencement du jeu de la vie.</b>
 * <p>
 * Un objet Jeu est caractérisé par :
 * <ul>
 * <li>Un monde du jeu de la vie .</li>
 *</ul>
 * </p>
 * @see Monde
 * @see MondeCirculaire
 * @see MondeFrontiere
 * @see JeuDeLaVie
 * @author arezki,nassim,koceila,serhat
 *
 */
public class Jeu {
	public int periode;
	public String deplacement;
	public String type;
	public int taille;
	public int option;
    public Jeu(){
     this.periode=0;
     this.deplacement="pas de déplacement";
     this.type="";
     this.taille=0;

	}
    /**
	 * Executer la commande passer en paramètre
	 * @param args
	 * 				Un tableau de chaine de caractère conténant les commandes.
	 * @throws IoException
	 * 				Lève une exception lorsque la commande est invalide.
	 * @throws ArgumentException
	 * @throws IOException 
	 */
	public Jeu(String[] args) throws IOException {
		this.periode=0;
		this.deplacement="pas de déplacement";
		this.type="";
		this.taille=0;
		try {
			switch (Choix(args)) {
			case 1:
				ListeNom();
				break;
			case 2:
				ListeOption();
				break;
			case 3:
				Monde u1 = choixUnivers(args[2]);
				SimulationJeu(Integer.parseInt(args[1]), u1);
				break;
			case 4:
				Monde u2 = choixUnivers(args[2]);
				EtudesAsymptote et = new EtudesAsymptote(u2, args[2]);
				Evolution(Integer.parseInt(args[1]), u2, et);
				break;
			case 5:
				System.out.println("choisissez le monde dont vous voulez calculez l'évolution des fichiers:");
				GenererHtml ml = new GenererHtml(args[2],Integer.parseInt(args[1]));
				ml.openFile("index.html");
				break;
            default: throw new ArgumentException("Erreur Arguments");
			}

		} catch (ArgumentException e) {
			System.out.println(e.getMessage());
		}

	}

	private final void ListeNom() {
		System.out.println("** LES MEMBRES DU GROUPE **");

		System.out.println("Mounsi Arezki  ");
		System.out.println("Maali nassim");
		System.out.println("Fenouch koceila");
		System.out.println("Serhat ...");
	}
	/**
	 * <p>Affiche les options du programme.</p>
	 *
	 */

	private final void ListeOption() {
		System.out
				.println("** java -jar JeuDeLaVie.jar -name : affiche les noms et prénoms des membres du groupe de projet**");
		System.out.println();
		System.out
				.println("** java -jar JeuDeLaVie.jar -h : rappelle la liste des options du programme  **");
		System.out.println();
		System.out
				.println("** java -jar JeuDeLaVie.jar -s d fichier.lif : éxécute une simulation du jeu d’une durée d affichant les configurations du jeu avec le numéro de génération");
		System.out.println();
		System.out
				.println("** java -jar JeuDeLaVie.jar -c max fichier.lif : calcule le type d’évolution du jeu avec ses caractéristiques (taille de la queue, période et déplacement), max représente la durée maximale de simulation pour déduire les résultats du calcul.");
		System.out.println();
		System.out
				.println("** java -jar JeuDeLaVie.jar -w max dossier : calcule le type d’évolution de tous les jeux contenus dans le dossier passé en paramètre et affiche les résultats sous la forme d’un fichier html");
		System.out.println();
	}

	public Monde choixUnivers(String file) {
		int mnd;
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("Monde Infinis Taper 1");
			System.out.println("*****************");
			System.out.println("Monde Circulaire Taper 2");
			System.out.println("*****************");
			System.out.println("Monde Frontière Taper 3");
			mnd = sc.nextInt();
		} while (mnd != 1 && mnd != 2 && mnd != 3);
		Monde monde;
		if (mnd == 1) {
			monde = new Monde(file, 1);
		} else {
			if (mnd == 2) {
				monde = new MondeCirculaire(file, 2);
			} else {
				monde = new MondeFrontiere(file, 3);
			}
		}
		return monde;
	}

	private int Choix(String[] args) throws ArgumentException {
		if (args.length == 1) {
			if (args[0].equals("-name")) {
				return 1;
			} else if (args[0].equals("-h")) {
				return 2;
			} else {
				throw new ArgumentException("Erreur arguments");
			}
		} else if (args.length == 3) {
			if (args[0].equals("-s") && args[1].matches("^[0-9]+$")) {
				return 3;
			} else if (args[0].equals("-c") && args[1].matches("^[0-9]+$")) {
				return 4;
			} else if (args[0].equals("-w") && args[1].matches("^[0-9]+$")) {
				return 5;
			} else {
				throw new ArgumentException("Erreur d'arguments");
			}
		} else {
			throw new ArgumentException("Erreur arguments");
		}

	}

	/**
	 * Execute une simulation d'un jeu de la vie sur une durée donné.
	 * @param dur
	 * 				Un entier correspondant au nombre d'évolution à éffectuer.
	 * @param monde
	 * 				 type de plateau du jeu de la vie. 
	 *@see Monde
	 *@see MondeCirculaire
	 *@see MondeFrontiere
     *@see JeuDeLaVie
	 *
	 */
	public void SimulationJeu(final int dur, final Monde monde) {


		final Timer timer = new Timer(300, new ActionListener() {

			int generation = 1;

			public void actionPerformed(ActionEvent e) {
					monde.display();
					System.out.println("génération numéro: " + generation);
					monde.nextGeneration();
				    generation++;
				if (generation == dur + 1) {
					System.exit(0);
				}
			}

		});
		timer.start();

		try {
			System.in.read();
		} catch (Exception e) {
			timer.stop();
		}
	}

	public void Evolution(final int max, final Monde monde, final EtudesAsymptote et) {

		
		final Timer timer = new Timer(100, new ActionListener() {


			int generation = 0;

			public void actionPerformed(ActionEvent e) {

				if (generation != max+1) {
					if (et.Mort(monde)) {
						System.out.println("taille de la queue:"
								+monde.getLemonde().getSize());
						System.out.println("type de jeux: Mort");
						System.exit(0);
					} else if (et.Stable(monde)) {
						System.out.println("taille de la queue:"
								+ monde.getLemonde().getSize());
						System.out.println("type de jeux: Stable");
						System.out.println("La période est de "
								+ et.calculePeriode(monde));
						System.out.println("Déplacement :"
								+ deplacement);
						System.exit(0);
					}else if (et.Oscillateur(monde)) {
						System.out.println("taille de la queue:"
								+ monde.getLemonde().getSize());
						System.out.println("type de jeux: Oscillateur");
						System.out.println("La période est de "
								+et.calculePeriode(monde));
						System.out.println("Déplacement :"
								+ deplacement);
						System.exit(0);
					} else if (et.Vaisseau(monde)) {
						System.out.println("taille de la queue:"
								+ monde.getLemonde().getSize());
						System.out.println("type de jeux: Vaisseau");
						System.out.println("La période est de "
								+et.calculePeriodeVaisseau(monde));
						System.out.println("Déplacement :"
								+et.deplacementVaisseau(monde));
						System.exit(0);
					}
				}else{
					System.out.println("taille de la queue:"
							+ monde.getLemonde().getSize());
					System.out.println("type de jeu: Inconnue");
					System.exit(0);

				}
				monde.nextGeneration();
				generation++;
			}
		});
		timer.start();

		try {
			System.in.read();
		} catch (Exception e) {
			timer.stop();
		}
	}
	public void EvolutionPourHtml(final int max, final Monde monde, final EtudesAsymptote et) {

		int generation = 0;
		while(generation!=max+2){
				if (generation != max+1) {
					if (et.Mort(monde)) {
						taille=monde.getLemonde().getSize();
						type="Mort";
						break;
					} else if (et.Stable(monde)) {
						taille=monde.getLemonde().getSize();
						type="Stable";
						periode=et.calculePeriode(monde);
						break;
					}else if (et.Oscillateur(monde)) {
						taille=monde.getLemonde().getSize();
						type="Oscillateur";
						periode=et.calculePeriode(monde);
						break;
					} else if (et.Vaisseau(monde)) {
						taille=monde.getLemonde().getSize();
						periode=et.calculePeriodeVaisseau(monde);
						deplacement=et.deplacementVaisseau(monde);
						type="Vaisseau";
						break;
					}
				}else{
					taille=monde.getLemonde().getSize();
					type="Inconnue";
					break;

				}
				monde.nextGeneration();
				generation++;
			}
	}

}