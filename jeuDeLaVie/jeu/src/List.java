package jeu;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Test;


/**
 * <b>class list c notre structure de donnees generique </b>
 * 
 * 
 * 
 * 
 */

public class List<T extends Comparable<T>> {

		private Maillon tete;
	  
		
		Maillon getTete() {
			return tete;
		}
      	 public void setTete(Maillon tete){
		 this.tete = tete;
	    }

	    public List(){
	        this.tete = null;
	    }
	/**
	 * verifie si la list est vide
	 * 
	 * @return boolean
	 */
	    public boolean isEmpty(){
	        return this.tete == null;
	    }

	    
	    
	   /**
	    * verifie l existance d un element dans la liste
	    * @param elem
	    * @return boolean
	    */
	    public boolean contains(T elem){
	        Maillon<T> selection = this.tete;
	        if (selection==null) {return false;}
	        while (selection!=null){
	            if(selection.getValeur().equals(elem))
	                return true;
	            selection = selection.getSuivant();
	        }
	        return false;
	    }

			 
		/**
		 * return la taille de la liste
		 * @return int
		 */
			    public int getSize() {
			    	int taille=0;
			        Iterator it = iterator();
			        while(it.hasNext()){
			        	it.next();
			        	taille++;
					}
			        return taille;
			    }

	    
	    /**
	     * supprimer un element de la liste
	     * @param c
	     * @return list
	     */
	    public boolean remove(T c) {
	        List<T> listeAvecElementAsupprimer= new List();
	      
	        if (this.isEmpty()) return false;
	        if(!contains(c)) {
	            return false;
	        }else {
	                for (Maillon m = tete; m != null; m = m.getSuivant()) {
	                   T e= (T) m.getValeur();
	                    if (e.equals(c)) {
	                        continue;
	                    }else listeAvecElementAsupprimer.add(e);
	                }
	            }
	        this.tete=listeAvecElementAsupprimer.tete;
	        return true;
	    }

	    /**
	     * <p>supprime cellule appartir de son etat</p>
	     * @param etat
	     * @return boolean
	     */
	    public boolean supprimerCelluleEtat(String etat) {
	        List listeAvecElementAsupprimer= new List();
	        if (this.isEmpty()) return false;
	                for (Maillon m = tete; m != null; m = m.getSuivant()) {
	                    Cellule cell = (Cellule) m.getValeur();
	                    if (cell.getEtat().equalsIgnoreCase(etat)) {
	                        continue;
	                    }else listeAvecElementAsupprimer.add(cell);
	                }
	        this.tete=listeAvecElementAsupprimer.tete;
	        return true;
	    }
	    


	    /**
	     * ajout un element qui n existe pas deja dans la liste </br>
	     * et return dalse sinon
	     * @param e
	     * @return boolean
	     */
	    public boolean add (T e ){

	        if (contains(e)) return false;
	        if (this.isEmpty()) {
	            this.tete = new Maillon(e, null);
	            return true;
	        } else {
	            if (this.tete.getValeur().compareTo(e) > 0) {
	                this.tete = new Maillon(e, this.tete); 
	                return true;
	            }
	            for (Maillon m = tete; m != null; m = m.getSuivant()) {
	                 if(m.getSuivant() != null) {
	                     if (m.getSuivant().getValeur().compareTo(e) > 0) {
	                         m.setSuivant(new Maillon(e, m.getSuivant())) ;
	                         return true;
	                     }
	                 }
	                if (m.getSuivant() == null) {
	                    m.setSuivant(new Maillon(e, null)) ; 

	                    return true;
	                }
	            }
	        }
	        return false;
	    }

	    /**
	     * 
	     * @return un iterator des elements de la liste.
	     */
	    public Iterator<T> iterator(){
	        return new Itr();
	    }

	   /**class Iterator 
	    * 
	    * @author mounsi
	    *
	    *
	    */
	    private class Itr implements Iterator<T>{
	        Maillon<T> maill;

	        public Itr(){
	            maill = List.this.tete;
	        }

	        public boolean hasNext() {
	            return maill!= null;
	        }

	        public T next() {
	            if(maill == null)
	                throw new NoSuchElementException();
	            T temp = maill.getValeur(); 
	            maill = maill.getSuivant(); 
	            return temp;
	        }
	    }

	    private class Maillon<T extends Comparable<T>> {

	        private T value;
	        private Maillon<T> next;

	        public Maillon(T value, Maillon<T> next){
	            this.value=value;
	            this.next=next;
	        }

	        public T getValeur(){
	            return value;
	        }

	        public void setValeur(T value) {
	            this.value = value;
	        }

	        public void setSuivant(Maillon<T> next) {
	            this.next = next;
	        }

	        public Maillon<T> getSuivant() {
	            return next;
	        }

	    }
	    /**
	     * 
	     * @param liste1
	     * @param liste2
	     * @return boolean
	     */
	    
	    public boolean equalsTwoList(List liste1, List liste2){
	    	if(liste1.getSize()!=liste2.getSize()){
	    		return false;
			}
			Iterator it1 = liste1.iterator();
		    Iterator it2 = liste2.iterator();
		    while(it1.hasNext() && it2.hasNext()){
		    	T c1 = (T)it1.next();
				T c2 = (T)it2.next();
				if(!(c1.equals(c2))){
					return false;
				}

			}
			return true;
	}


	@Override
	public String toString() {
		return "List [tete=" + tete.getValeur() + " size=" + getSize()+"]";
	}

	
	@Test
	public void testList(){

		List<Cellule> l = new List<Cellule>();


		Cellule cc=   new Cellule(new Coordonnees(2,2),"live");

		l.add(cc);;

		l.add(new Cellule(new Coordonnees(1,0),"live"));
		Cellule c2=   new Cellule(new Coordonnees(55,0),"live");
		//l.supprimerMaillon(c2);;
         l.add(c2);
		Iterator it = l.iterator();
		Maillon t = l.getTete();
		// System.out.println(l);
		while(it.hasNext()){
			System.out.println(it.next());
		
		}


	}
	
}


