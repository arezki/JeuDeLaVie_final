package jeu;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;
/**
 *<b> Classe Monde représente le monde infini , elle  contient 2 List : lemonde
 * (qui contient les cellules vivantes) et celmortes (qui contient les cellules mortes)
 * et aussi des attributs minligne mincolonne nbligne nbcolonne qui représentent la 
 * taille de notre monde au début de l'exécution .<b>
 * 
 * 
 * @author arezki,nassim
 */
public class Monde {

	/**
	 * La liste des cellules vivantes
	 */
	private List<Cellule> lemonde;

	/**
	 * La liste des cellules mortes
	 */
	private List<Cellule> celmortes;

	/**
	 * La ligne de la 1ère cellule du fichier lif. Sert pour l'affichage.
	 */
	private int minligne;

	/**
	 * La colonne de la 1ère cellule du fichier lif. Sert pour l'affichage.
	 */
	private int mincolonne;

	/**
	 * Le nombre de lignes maximales du fichier lif. Sert pour l'affichage.
	 */
	private int nbligne;

	/**
	 * Le nombre de colonnes maximales du fichier lif. Sert pour l'affichage.
	 */
	private int nbcolonne;

	/**
	 * Constructeur Univers.
	 * <p>
	 * A la construction d'un Univers, on initialise les deux listes.
	 * </p>
	 * 
	 */
	public Monde() {
		lemonde = new List<Cellule>();
		celmortes = new List<Cellule>();
	}

	/**
	 * Constructeur Monde à partir d'un fichier lif.
	 * <p>
	 * A la construction d'un Monde, on initialise la 1ere cellule selon le
	 * monde: l'id_monde 1 représente le monde infini on initialise la 1ère
	 * cellule au coordonnée notée après #P, l'id_monde 2 représente le monde
	 * circulaire on initialise la 1ère cellule au coordonnée (0,0) et
	 * l'id_monde 3 représente le monde frontiere on initialise la 1ère cellule
	 * au coordonnée (0,0).
	 * </p>
	 * 
	 */

	public Monde(String file, int id_monde) {

		lemonde = new List<Cellule>();
		celmortes = new List<Cellule>();
		this.nbligne = 0;
		this.nbcolonne = 0;

		File f = new File(file);
		Scanner sc = null;
		int numligne = 0, numcolonne = 0, maxligne = 0;

		try {
			sc = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String ligne = sc.nextLine();

		boolean inutile = true;

		while (sc.hasNextLine() && inutile) {
			if (ligne.charAt(0) == '#' && ligne.charAt(1) != 'P') {
				ligne = sc.nextLine();
			} else {
				inutile = false;
			}
		}

		Scanner st = new Scanner(ligne);
		String c1;
		c1 = st.next();
// throw Exception in case it's not an Integer
		if (c1.equals("#P")) {
			if (id_monde == 1) {
				try{
					numligne = Integer.parseInt(st.next());
					this.minligne = numligne;
					numcolonne = Integer.parseInt(st.next());
					this.mincolonne = numcolonne;}catch(Exception e){System.out.println("Error input ");}
			} else {
				minligne = 0;
				mincolonne = 0;
			}
		}
		numligne = minligne;
		while ((sc.hasNextLine() && (ligne = sc.nextLine()) != null && ligne
				.length() > 0)) {
			numcolonne = this.mincolonne;
			for (int i = 0; i <= ligne.length() - 1; i++) {
				if (ligne.charAt(i) == '*') {
					Cellule a = new Cellule(new Coordonnees(numligne,
							numcolonne), "vivante");
					lemonde.add(a);
				}

				numcolonne++;

				if (ligne.length() > this.nbcolonne) {
					this.nbcolonne = ligne.length();

				}

			}
			numligne++;
			maxligne++;
			if (maxligne > this.nbligne) {
				this.nbligne = maxligne;
			}

		}
	}
	/**
	 * Retourne la cellule dont les coordonnées sont égales au coordonnées
	 * passées en paramètre. Si la cellule n'existe pas, on la crée.
	 * 
	 * @param s
	 *            Les coordonnées à chercher.
	 * 
	 * @return la cellule correspondante au coordonnées passées en paramètre.
	 * 
	 * 
	 */
	public Cellule cellExists(Coordonnees s) {
		Iterator<Cellule> it =null;
		if (s.valide) {
			it = this.lemonde.iterator();
			while (it.hasNext()) {
				Cellule cel1 = (Cellule) it.next();
				if ((cel1.getCoord().x == s.x) && (cel1.getCoord().y == s.y) && (cel1.getEtat() == "vivante")) {
					return cel1;
				}
				if(cel1.getCoord().x>s.x){
					break;
				}
			}
		}
		return new Cellule(new Coordonnees(s.x, s.y), "morte");
	}

	/**
	 * Ajoute les 8 cellules voisines de chaques cellules vivantes. Puis on
	 * compte les cellules vivantes parmi les 8 pour leur attribuer un état
	 * suivant.
	 * @author nassim
	 */

	public void addCellulesMortes() {
		int i, j, dx, dy;
		int nb = 0;
		Iterator<Cellule> it = this.lemonde.iterator();
		List liste =null;
		while (it.hasNext()) {
			liste = new List();
			Cellule cell = (Cellule)it.next();
			if (cell.getEtat().equals("vivante")) {
				nb = 0;
				dx = cell.getCoord().x;
				dy = cell.getCoord().y ;
				for (i= (dx-1); i <=(dx+1); i++) {
					for (j = (dy-1); j <= (dy+1); j++) {
						if (i==dx && j==dy) {
                          continue;
						}else{
							liste.add(cellExists(checkCoord(i,j)));
						}
					}
				}
				Iterator itl = liste.iterator();
				while(itl.hasNext()){
					Cellule cel1= (Cellule)itl.next();
					if ((cel1.getEtat().equalsIgnoreCase("morte")) && !(this.celmortes.contains(cel1))) {
						this.celmortes.add(cel1);
					}else
					if (cel1.getEtat().equalsIgnoreCase("vivante")){
						nb++;
					}
				}
				if (nb == 2 || nb == 3) {
					cell.setEtat_suivant(cell.getEtat());
				}else {
					cell.setEtat_suivant("morte");
				}
			}
		}
	}

	/**
	 * Ajoute les 8 cellules voisines de chaques cellules mortes. Puis on compte
	 * les cellules vivantes parmi les 8 pour leur attribuer un état suivant.
	 * 
	 */
	public void verifierListCell() {
		int i, j, dx, dy;
		int cp = 0;
		Iterator<Cellule> it = this.celmortes.iterator();
		List liste=null;
		while (it.hasNext()) {
			liste=new List();
			Cellule cell = (Cellule) it.next();
			cp = 0;
				dx = cell.getCoord().x;
				dy = cell.getCoord().y ;
				for (i= dx-1; i <=dx+1; i++) {
					for (j = dy - 1; j <= dy + 1; j++) {
						if (i==dx && j==dy) {
							continue;
						}else{
							liste.add(cellExists(checkCoord(i, j)));
						}
					}
				}
			Iterator itl = liste.iterator();
			while(itl.hasNext()){
				Cellule cel1= (Cellule)itl.next();
				if (cel1.getEtat().equalsIgnoreCase("vivante")){
					cp++;
				}
			}
			if (cp == 3) {
				cell.setEtat_suivant("vivante");
			}else{
					cell.setEtat_suivant(cell.getEtat());
			}
		}
	}

	/**
	 * Change les états des cellules. Supprime dans la liste des cellules
	 * vivantes, les cellules devenues mortes. Ajoute dans la liste des cellules
	 * vivantes, les cellules devenues vivantes et les supprime de la liste des
	 * cellules mortes.
	 * @author arezki
	 */

	public void changeState() {
		Iterator<Cellule> it = this.lemonde.iterator();
		while (it.hasNext()) {
			Cellule cell = (Cellule)it.next();
			cell.setEtat(cell.getEtat_suivant());
			cell.setEtat_suivant(null);
		}
		this.lemonde.supprimerCelluleEtat("morte");
		it= this.celmortes.iterator();
		while (it.hasNext()) {
			Cellule cell = (Cellule) it.next();
			cell.setEtat(cell.getEtat_suivant());
			cell.setEtat_suivant(null);
			if (cell.getEtat().equals("vivante")) {
				this.lemonde.add(new Cellule(new Coordonnees(cell.getCoord().x, cell
						.getCoord().y), cell.getEtat()));

			}
		}
		this.celmortes.supprimerCelluleEtat("vivante");

	}

	/**
	 * Permet d'afficher l'univers sous forme de tableau. Les cellules vivantes
	 * sont représentées par des "*". Pour le monde infini on affiche que la
	 * taille maximale du fichier lif.
	 * @author arezki
	 */
	public void display() {
		for (int i = minligne; i < (minligne + nbligne) ; i++) {
			for (int j = mincolonne; j < (mincolonne + nbcolonne); j++) {
				if (isPresent(i, j)) {
					System.out.print(" * ");
				} else {
					System.out.print("   ");
				}
			}
			System.out.println();
		}
	}

	/**
	 * Permet de tester si la cellule au coordonnées i et j est présente dans la
	 * liste des cellules vivantes. Comme la liste est toujours triée, on arrête
	 * la boucle dès que l'on a dépassé la ligne correspondante.
	 * 
	 * @param i
	 *            la ligne
	 * 
	 * @param j
	 *            la colonne
	 * 
	 * @return true si la cellule est présente dans la liste, false sinon.
	 * @author arezki
	 */
	public boolean isPresent(int i, int j) {
		Iterator<Cellule> it = this.lemonde.iterator();
		while (it.hasNext()) {
			Cellule a = it.next();
			if (a.getCoord().x > i)
				break;
			if (a.getCoord().x == i && a.getCoord().y == j) {
				return true;
			}
		}
		return false;
	}
     public void afficherLeMonde(){
		Iterator<Cellule> it = this.lemonde.iterator();
		while(it.hasNext()){
			Cellule cell= (Cellule)it.next();
			System.out.println(cell);
		}
	 }
	/**
	 * Retourne la coordonnées construite à partir des paramètres.
	 * 
	 * @param a
	 *            la ligne
	 * 
	 * @param b
	 *            la colonne
	 * 
	 * @return la coordonnée avec la ligne a et la colonne b.
	 */
	public Coordonnees checkCoord(int a, int b) {
		return new Coordonnees(a, b);
	}

	/**
	 * Calcule une nouvelle génération en utilisant les différentes méthodes.
	 */
	public void nextGeneration() {
		this.addCellulesMortes();
		this.verifierListCell();
		this.changeState();
	}
	/**
	 * 
	 * @return la liste des cellules vivantes.
	 * 
	 */
	public List<Cellule> getLemonde() {
		return this.lemonde;
	}

	/**
	 * 
	 * @return la liste des cellules mortes.
	 * 
	 */
	public List<Cellule> getCelmortes() {
		return this.celmortes;
	}

	/**
	 * 
	 * @return la ligne de la 1ère cellule du fichier lif.
	 * 
	 */

	public int getMinligne() {
		return this.minligne;
	}

	/**
	 * 
	 * @return la colonne de la 1ère cellule du fichier lif.
	 * 
	 */
	public int getMincolonne() {
		return this.mincolonne;
	}

	/**
	 * 
	 * @return le nombre de lignes maximales du fichier lif.
	 * 
	 */
	public int getNbligne() {
		return this.nbligne;
	}

	/**
	 * 
	 * @return le nombre de colonnes maximales du fichier lif.
	 * 
	 */
	public int getNbcolonne() {
		return nbcolonne;
	}

	/**
	 * Met à jour la liste des cellules vivantes.
	 * 
	 * @param s
	 *            La nouvelle liste.
	 */
	public void setLemonde(List<Cellule> s) {
		this.lemonde = s;
	}

	/**
	 * Met à jour la liste des cellules mortes.
	 * 
	 * @param celmortes
	 *            La nouvelle liste.
	 */
	public void setCelmortes(List<Cellule> celmortes) {
		this.celmortes = celmortes;
	}

	/**
	 * Met à jour la ligne de la 1ère cellule.
	 * 
	 * @param minligne
	 *            La nouvelle ligne
	 */
	public void setMinligne(int minligne) {
		this.minligne = minligne;
	}

	/**
	 * Met à jour la colonne de la 1ère cellule.
	 * 
	 * @param mincolonne
	 *            La nouvelle colonne
	 */
	public void setMincolonne(int mincolonne) {
		this.mincolonne = mincolonne;
	}

	/**
	 * Met à jour le nombre maximales de lignes du fichier lif.
	 * 
	 * @param nbligne
	 *            Le nouveau nombre
	 */
	public void setNbligne(int nbligne) {
		this.nbligne = nbligne;
	}

	/**
	 * Met à jour le nombre maximales de colonnes du fichier lif.
	 * 
	 * @param nbcolonne
	 *            Le nouveau nombre
	 */
	public void setNbcolonne(int nbcolonne) {
		this.nbcolonne = nbcolonne;
	}
}