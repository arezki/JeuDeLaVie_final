package jeu;
/**
 * <b>
 * Classe MondeCirculaire  , cette classe hérite de
 * Monde et donc a les memes attributs que Monde , la méthode checkCoord est redéfinie. </b>
 *@author nassim
 */
public class MondeCirculaire extends Monde {

	public MondeCirculaire() {
		super();
	}

	public MondeCirculaire(String file, int id_monde){
		super(file,id_monde);
	}
	
	
	/**
	 * Retourne la coordonnée construite à partir des paramètres. Si la coordonnée dépasse du bord, 
	 * on retourne la coordonnée de la cellule du bord opposée.
	 * 
	 * @param l
	 *            la ligne
	 * @param c
	 *            la colonne
	 *            
	 * @return La ligne et la colonne sous forme de coordonnées
	 */
	public Coordonnees checkCoord(int l, int c) {
		int dx=(l+this.getNbligne())%this.getNbligne();
		int dy=(c+this.getNbcolonne())%this.getNbcolonne();
		return new Coordonnees(dx,dy);
	}
}